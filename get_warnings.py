import click
import json
import requests
import zipfile
import io

from hashlib import sha256
from models.tracewarningpackage_pb2 import TraceWarningPackage
from models.checkin_pb2 import CheckInRecord
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad
from hashlib import sha256

def get_warnings():

    base_api = "http://localhost:8003/cwa/version/v2/twp/country/DE/hour" 
    
    # get latest hour
    res = requests.get(url=base_api)
    latest = res.json()['latest']
    # print(latest)

    # get warnings for latest out
    res = requests.get(url=f"{base_api}/{latest}") #, stream=True)
    z = zipfile.ZipFile(io.BytesIO(res.content))
    twp = TraceWarningPackage()
    twp.ParseFromString(z.read("export.bin"))

    print("found", len(twp.checkInProtectedReports), "reports")
    return(twp.checkInProtectedReports)


def decrypt_checkin_record(record, iv, location_id):

    key = sha256(bytes.fromhex("4357412d454e4352595054494f4e2d4b4559") + location_id).digest()
    decipher = AES.new(key, AES.MODE_CBC, iv)
    record = decipher.decrypt(record)
    record = unpad(record, AES.block_size)

    checkin_record = CheckInRecord()
    checkin_record.ParseFromString(record)

    return checkin_record


@click.command()
@click.argument('location-data', type=click.Path(exists=True))
@click.argument('infections-data', type=click.Path(exists=True))
def main(location_data, infections_data):

    with open(location_data, encoding='utf-8') as f:
        locations = json.load(f)

    with open(infections_data, encoding='utf-8') as f:
        infections = json.load(f)

    warnings = get_warnings()
    # print(warnings)

    for entry in locations:
        query_location_id = entry['location_id'].encode(encoding='latin-1')
        print("checking for", entry['description'], "with id:", query_location_id, "\n")

        for warning in warnings:

            warning_location_id = warning.locationIdHash
            # double hash query location
            query_location_id_db_hashed = sha256(query_location_id).digest()
            
            if not warning_location_id == query_location_id_db_hashed:
                continue

            record = decrypt_checkin_record(
                warning.encryptedCheckInRecord, 
                warning.iv,
                query_location_id
            )

            start = record.startIntervalNumber * 10*60
            end = start + record.period * 10*60
            decoded_location_id = query_location_id.decode(encoding='latin-1')

            if decoded_location_id not in infections.keys():
                infections[decoded_location_id] = {
                    "description": entry["description"],
                    "address": entry["address"],
                    "infectionsCnt": 1,
                    "infectionsTimes":
                        [{
                            "start": start,
                            "end": end
                        }]
                }
            else:
                infection_entry = infections[decoded_location_id]
                infection_times = infection_entry["infectionsTimes"]
                infection_already_present = False
                
                for times in infection_times:
                    if times["start"] == start and times["end"] == end:
                        infection_already_present = True
                        break

                if infection_already_present:
                    print("infection already tracked. Skipping this one.")
                    continue

                infection_entry["infectionsCnt"] += 1
                infection_entry["infectionsTimes"].append({
                    "start": start,
                    "end": end
                })

                infections[decoded_location_id] = infection_entry

    # write changes back to infections.json        
    with open(infections_data, "w", encoding='utf-8') as f:
        f.write(json.dumps(infections, ensure_ascii=False, indent=4))


if __name__ == '__main__':
    main()