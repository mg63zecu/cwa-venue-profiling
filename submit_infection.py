import random
import time
import click
import hmac
import json
import requests

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

from datetime import datetime, timedelta
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from hashlib import sha256

from models.submission_pb2 import SubmissionPayload
from models.infections_pb2 import TemporaryExposureKey
from models.checkin_pb2 import CheckInRecord, CheckInProtectedReport

class ExposureKeys():

    def __init__(self):

        self.risk_level = 6
        self.rolling_period = 144
        self.report_type = 1      

    def generate(self, amount=10):
    
        now = datetime.fromtimestamp(time.time())
        today_midnight = datetime(now.year, now.month, now.day)
        today = today_midnight - timedelta(days=amount)

        keys = []
        for idx in range(amount):
            tek = TemporaryExposureKey()
            tek.key_data = random.randbytes(16)
            tek.transmission_risk_level = self.risk_level
            tek.rolling_start_interval_number = int((today.timestamp()/600) + (self.rolling_period * idx))
            tek.rolling_period = self.rolling_period
            tek.report_type = self.report_type
            tek.days_since_onset_of_symptoms = 0
            keys.append(tek)
        
        return keys


def generate_checkin(time, duration, location_id):
        
    checkin_record = CheckInRecord()

    iv = random.randbytes(16)
    enckey = sha256(bytes.fromhex("4357412d454e4352595054494f4e2d4b4559") + location_id).digest()

    start = time
    end_time = start + duration
    start_time_agg = int(start / (10 * 60))
    end_time_agg = int(end_time / (10 * 60))
    period = end_time_agg - start_time_agg

    # print("start_time_agg:", start_time_agg, "end:", end_time_agg, "period", period)

    checkin_record.startIntervalNumber = start_time_agg
    checkin_record.period = period
    checkin_record.transmissionRiskLevel = 2
    checkin_record_string = checkin_record.SerializeToString()

    msg = pad(checkin_record_string, AES.block_size)
    cipher = AES.new(enckey, AES.MODE_CBC, iv)
    cipher_text = cipher.encrypt(msg)
    encrypted_record = cipher_text

    mackey = sha256(bytes.fromhex("4357412d4d41432d4b4559") + location_id).digest()
    mac = hmac.new(mackey, iv + encrypted_record, sha256).digest()

    report = CheckInProtectedReport()
    report.locationIdHash = sha256(location_id).digest() # hashed twice
    report.iv = iv
    report.encryptedCheckInRecord = encrypted_record
    report.mac = mac

    return [report]


def build_payload(keys, checkin_reports):
    
    payload = SubmissionPayload()
    payload.origin = "DE"
    payload.consentToFederation = True
    payload.requestPadding = bytes(0 for _ in range(100))

    for c in ["DE"]:
        payload.visitedCountries.append(c)
    for c in checkin_reports:
        payload.checkInProtectedReports.append(c)
    for k in keys:
        payload.keys.append(k)

    return payload


@click.command()
@click.argument('location-data', type=click.Path(exists=True))
@click.argument('location-name', type=click.STRING)
def main(location_data, location_name):

    with open(location_data, encoding='utf-8') as f:
        data = json.load(f)

    for entry in data:
        if entry['description'] == location_name:
            location_id = entry['location_id'].encode(encoding='latin-1')
            break
    
    exposure_keys = ExposureKeys().generate(10)
    checkin_reports = generate_checkin(int(time.time()), 60*30, location_id)
    payload = build_payload(exposure_keys, checkin_reports)
    payload = payload.SerializeToString()

    headers = {
        'CWA-Authorization': 'edc07f08-a1aa-11ea-bb37-0242ac130002',
        'CWA-Fake': '0',
        'Content-Type': 'application/x-protobuf',
    }

    url = 'https://localhost:8000/version/v1/diagnosis-keys'
    res = requests.post(url=url, data=payload, headers=headers, verify=False)
    print(res)


if __name__ == "__main__":
    main()
