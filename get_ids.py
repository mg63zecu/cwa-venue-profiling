import json
import cv2
from os import listdir
from os.path import isfile, join
import base64
import click

from models.qr_pb2 import QRCodePayload
from hashlib import sha256

def location_from_uri(uri):

    print(uri)
    data = uri.split("#")[-1]
    data = data.strip()
    data = data + ("="*2)
    data = data.encode("ascii")
    data = base64.urlsafe_b64decode(data)
    location_id = sha256("CWA-GUID".encode() + data).digest()

    qrcode = QRCodePayload()
    qrcode.ParseFromString(data)

    return qrcode.locationData, location_id

@click.command()
@click.argument('qrpath', type=click.Path(exists=True))
def main(qrpath):

    onlyfiles = [f for f in listdir(qrpath) if isfile(join(qrpath, f))]
    map = []
    for path in onlyfiles:
        path = join(qrpath, path)
        img = cv2.imread(path)

        # initialize the cv2 QRCode detector
        detector = cv2.QRCodeDetector()

        # detect and decode
        data, _, _ = detector.detectAndDecode(img)

        location_data, location_id = location_from_uri(data)
        data = {}
        data["version"] = location_data.version
        data["description"] = location_data.description
        data["address"] = location_data.address
        data["location_id"] = location_id.decode(encoding='latin-1')
        map.append(data)


    print(map)
    with open("locations.json", "w", encoding='utf-8') as file:
        file.write(json.dumps(map, ensure_ascii=False, indent=4))

if __name__ == "__main__":
    main()